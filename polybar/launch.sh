#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar
polybar top -c ~/.config/polybar/config.ini &

# When primary Monitor is off
# pri_monitor=$(xrandr -q | grep eDP1)

# # Check Whether my eDP1 is connected
# pri_monitor_status=$(echo $pri_monitor | awk '{print $2}')
# if [[ $pri_monitor_status == connected* ]]; then
# 	polybar top -c ~/.config/polybar/config.ini &
# fi

# # SetUp the external monitor
# # Primary Monitor as eDP1
# # External Monitor as HDMI1
# ext_monitor=$(xrandr -q | grep HDMI1)

# # check Whether my HDMI1 is connected
# ext_monitor_status=$(echo $ext_monitor | awk '{print $2}')
# if [[ $ext_monitor_status == connected* ]]; then
# 	polybar top_extend -c ~/.config/polybar/config.ini &
# fi

